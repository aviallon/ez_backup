# ez_backup

## Usage
~~~
usage: ez_backup [-h] [--threads THREADS] [--exclude EXCLUDE [EXCLUDE ...]] [--debug] --output OUTPUT inputs [inputs ...]

positional arguments:
  inputs

optional arguments:
  -h, --help            show this help message and exit
  --output OUTPUT, -o OUTPUT
                        Output file to write to

options:
  --threads THREADS, --jobs THREADS, -j THREADS
                        Number of threads to run in the different stages
  --exclude EXCLUDE [EXCLUDE ...]
                        Exclude specified files from backup
  --debug               Print debug information
~~~