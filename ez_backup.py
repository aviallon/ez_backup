#!/bin/env python3
# -*- coding: utf-8 -*-

import multiprocessing.pool
import pathlib
import functools
import re
import sys
import time
from typing import Callable, Any, Union, Optional, TypeVar


class DictDefault(dict):
    def __init__(self, *args, default_value, **kwargs):
        self.default_value = default_value
        super().__init__(*args, **kwargs)

    def __missing__(self, key):
        return self.default_value


Number = TypeVar('Number', float, int)


def auto_prefix_value(value: Number, force_prefix: str = None) -> tuple[float, str]:
    _prefix = {'y': 1e-24,  # yocto
               'z': 1e-21,  # zepto
               'a': 1e-18,  # atto
               'f': 1e-15,  # femto
               'p': 1e-12,  # pico
               'n': 1e-9,  # nano
               'u': 1e-6,  # micro
               'm': 1e-3,  # mili
               'c': 1e-2,  # centi
               'd': 1e-1,  # deci
               '': 1e0,  # No prefix
               'k': 1e3,  # kilo
               'M': 1e6,  # mega
               'G': 1e9,  # giga
               'T': 1e12,  # tera
               'P': 1e15,  # peta
               'E': 1e18,  # exa
               'Z': 1e21,  # zetta
               'Y': 1e24,  # yotta
               }

    if force_prefix is not None:
        return round(value / _prefix[force_prefix], 3), force_prefix

    for prefix, ratio in _prefix.items():
        scaled_value = value / ratio
        # debug(f"{prefix=} {scaled_value=} {value=} {ratio=}")

        if 1e3 > scaled_value >= 1:
            return round(scaled_value, 3), prefix
    return value, ''


def trace_call(*glob_args, print_res=False):
    def _trace_call(func: Callable):
        import threading

        @functools.wraps(func)
        def _func(*args, **kwargs):
            if trace_call.debug:
                thread_id = threading.get_ident()
                kwargs_signature = [f"{kwarg[0]}={repr(kwarg[1])}" for kwarg in kwargs.items()]
                args_signature = [repr(arg) for arg in args]
                func_signature = f"{func.__name__}({', '.join(args_signature + kwargs_signature)})"

                _func.threads[thread_id] += 1
                print(f"{time.time():<20}: "
                      f"({thread_id % 100:<3},{_func.threads[thread_id]:<3}) "
                      f"START {func_signature}\n", end='')
            res = func(*args, **kwargs)
            if trace_call.debug:
                res_msg = f" -> {res}" if print_res else ""
                print(f"{time.time():<20}: "
                      f"({thread_id % 100:<3},{_func.threads[thread_id]:<3}) "
                      f"  END {func_signature}{res_msg}\n", end='')
                _func.threads[thread_id] -= 1
            return res

        _func.threads: dict[int, int] = DictDefault(default_value=0)

        return _func

    if len(glob_args):
        return _trace_call(glob_args[0])
    else:
        return _trace_call


trace_call.debug: bool = False


def debug(*args, **kwargs):
    if debug.enabled:
        print("Debug:", *args, **kwargs)


debug.enabled: bool = False


@trace_call
def find_all_files(directory: pathlib.Path, thread_pool: multiprocessing.pool.Pool, *,
                   use_threads: bool = False,
                   exclude_patterns: list[re.Pattern] = None, ) -> list[pathlib.Path]:
    """
    Multi-threaded recursive file listing
    @:return list of all files in directory
    """

    if exclude_patterns is None:
        exclude_patterns = []

    @trace_call
    def _find(file_list: list[pathlib.Path], current_file: pathlib.Path) -> list[pathlib.Path]:

        for pattern in exclude_patterns:
            if re.match(pattern, current_file.name) is not None:
                return []

        if current_file.is_dir():
            res = file_list + [current_file]

            if use_threads:
                files = list(current_file.iterdir())
                temp = thread_pool.map(lambda _f: _find([], _f),
                                       files,
                                       chunksize=32)

                for item in list(temp):
                    res += item
            else:
                for f in current_file.iterdir():
                    res += _find([], f)

            return res

        else:
            return [current_file]

    return _find([], directory)


def get_file_hash(file: pathlib.Path) -> str:
    import hashlib

    hash_result: str = ""

    def _md5(data: bytes) -> str:
        return hashlib.md5(data).hexdigest()

    if file.is_file():
        try:
            hash_result = _md5(file.open('rb').read())
        except FileNotFoundError as e:
            print(f"FileNotFoundError: {e.filename}")
    else:
        pass
        # file_list = ','.join([f.name for f in file.iterdir()])
        # hash_result = _md5(file_list.encode('utf-8'))

    return hash_result


def _file_info(file_list: list[pathlib.Path], pool: multiprocessing.pool.ThreadPool) \
        -> list[dict[str, Union[float, str, pathlib.Path]]]:
    hashes = list(pool.map(get_file_hash, file_list))
    recursive_sizes = get_recursive_size(file_list, pool=pool)

    sizes = []
    for f in file_list:
        try:
            sizes += [f.stat().st_size]
        except FileNotFoundError as e:
            print(f"FileNotFoundError: {e.filename}")
            sizes += [0]

    infos = [{"file": file_list[i],
              "recursive_size": recursive_sizes[file_list[i].as_posix()],
              "size": sizes[i],
              "hash": hashes[i]}
             for i in range(len(file_list))]

    return infos


@trace_call(print_res=True)
def get_recursive_size(file_list: list[pathlib.Path], pool: multiprocessing.pool.ThreadPool) -> dict[str, int]:
    sizes: dict[str, int] = DictDefault(default_value=0)
    for file in file_list:
        file_size = 0
        try:
            file_size = file.stat().st_size
        except FileNotFoundError as e:
            print(f"FileNotFoundError: {e.filename}")

        sizes[file.as_posix()] += file_size
        for parent in file.parents:
            sizes[parent.as_posix()] += file_size
            debug(f"{file.as_posix()} adds {file_size} size to parent {parent} -> {sizes[parent.as_posix()]}")

    return sizes


def _create_archive(infos: list[dict[str, Any]],
                    archive_path: pathlib.Path,
                    index_path: pathlib.Path,
                    hashes_path: pathlib.Path,
                    threads: int = 1) -> None:
    import tarfile

    temporary_archive_path = archive_path.with_name(f".{archive_path.name}")

    uncompressed_archive_path = temporary_archive_path
    archive_suffix = temporary_archive_path.suffixes[-1]
    if archive_suffix != ".tar":
        uncompressed_archive_path = temporary_archive_path.with_suffix("")

    write_mode = "w"
    compressed_message = ""
    natively_supported_archive_format = [".gz", ".bz2", ".xz"]

    if archive_suffix in natively_supported_archive_format:
        write_mode = f"w:{archive_suffix[1:]}"
        compressed_message = "compressed"

    if archive_suffix in [".zst"]:
        try:
            import zstandard
        except (ImportError, ModuleNotFoundError):
            error_message = f"Couldn't import zstandard module, please install it or choose " \
                            f"a supported compression format: {', '.join(natively_supported_archive_format)}"
            print(error_message, file=sys.stderr)
            exit(1)
            # raise RuntimeError(error_message)

    print(f"Creating {compressed_message} archive...")

    def _print(*args, **kwargs):
        print(*args, **kwargs, end=f"{' ':>30}\r")

    with tarfile.open(uncompressed_archive_path, mode=write_mode) as archive:
        size = 0
        total_size = infos[0]['recursive_size']
        scaled_total_size, size_prefix = auto_prefix_value(total_size)
        total_count = len(infos)
        for i, file in enumerate(infos):
            size += file['size']
            scaled_size, _ = auto_prefix_value(size, force_prefix=size_prefix)
            _print(f"Progress: {i + 1}/{total_count} files ({(i + 1) * 100 // total_count}%) "
                   f"- {scaled_size:.3f}/{scaled_total_size:.3f} {size_prefix}B ({size * 100 // total_size}%)")
            archive.add(file['file'], recursive=False)

        print()
        archive.add(index_path, arcname=".tar_index")
        archive.add(hashes_path, arcname=".tar_hashes.md5")

    if not len(compressed_message):
        print("Compressing archive...")

        if archive_suffix == ".gz":
            import gzip

            with gzip.open(temporary_archive_path, mode='wb', compresslevel=6) as compressed_archive, \
                    open(uncompressed_archive_path, mode='rb') as uncompressed_archive:
                compressed_archive.write(uncompressed_archive.read())

            uncompressed_archive_path.unlink()

        elif archive_suffix == ".zst":
            import zstandard
            cctx = zstandard.ZstdCompressor(threads=threads)
            chunk_size = 2 << 16
            uncompressed_archive_size = uncompressed_archive_path.stat().st_size
            with open(uncompressed_archive_path, mode='rb') as uncompressed_archive, \
                    open(temporary_archive_path, mode='wb') as compressed_archive:
                compressor = cctx.stream_writer(compressed_archive)
                data_read = 0
                while data := uncompressed_archive.read(chunk_size):
                    data_read += len(data)
                    _print(f"Compression {round(100 * data_read / uncompressed_archive_size, 2):.2f}%")
                    compressor.write(data)
                    compressor.flush()

                compressor.flush(zstandard.FLUSH_FRAME)
                compressor.close()
                print()
                # cctx.copy_stream(uncompressed_archive, compressed_archive)

            uncompressed_archive_path.unlink()

    print("Moving files...")
    temporary_archive_path.rename(archive_path)


def main():
    import argparse
    import psutil
    import pathlib
    import multiprocessing

    parser = argparse.ArgumentParser("ez_backup")

    options = parser.add_argument_group("options")

    options.add_argument("--threads", "--jobs", "-j", type=int, default=psutil.cpu_count(),
                         help="Number of threads to run in the different stages")

    options.add_argument("--exclude", type=str, nargs='+', default=[],
                         help="Exclude specified files from backup")

    options.add_argument("--debug", default=False, action="store_true",
                         help="Print debug information")

    parser.add_argument("--output", "-o", type=pathlib.Path, required=True,
                        help="Output file to write to")

    parser.add_argument("inputs", type=pathlib.Path, nargs='+')

    args = parser.parse_args()

    args.output: pathlib.Path
    args.threads: int
    args.inputs: list[pathlib.Path]
    args.exclude: list[str]

    trace_call.debug = args.debug
    debug.enabled = args.debug

    exclude_patterns = [re.compile(pattern) for pattern in args.exclude]

    thread_pool = multiprocessing.pool.ThreadPool(processes=args.threads)

    debug(f"Input files: {args.inputs}")
    debug(f"Threads: {args.threads}")
    debug(f"Output: {args.output}")

    print(f"Listing files in {', '.join(str(x) for x in args.inputs)}...")

    files: list[pathlib.Path] = []
    for input_file in args.inputs:
        files += find_all_files(directory=input_file, thread_pool=thread_pool, use_threads=False,
                                exclude_patterns=[re.compile(r"\.tar_{index|hashes}.*")] + exclude_patterns)

    debug(files)
    debug(f"{len(files)=}")

    file_infos = _file_info(files, thread_pool)
    debug(f"{file_infos=}")

    total_size = file_infos[0]['recursive_size']
    scaled_total_size, size_prefix = auto_prefix_value(total_size)
    debug(f"{total_size=}")

    print(f"Archiving {len(files)} files, totalizing {scaled_total_size:.3f} {size_prefix}bytes.")

    def _write_hashes(infos: list[dict[str, Any]], file_path: pathlib.Path) -> None:
        standard_hash_content = ""
        for file_info in infos:
            if len(file_info["hash"]):
                standard_hash_content += f"{file_info['hash']}\t{file_info['file'].as_posix()}\n"

        with open(file_path, "w") as hash_file:
            hash_file.write(standard_hash_content)

    output_dir = args.output.parent
    hash_file_path = output_dir.joinpath(".tar_hashes.md5")
    _write_hashes(file_infos, hash_file_path)

    def _write_index(infos: list[dict[str, Any]], file_path: pathlib.Path) -> None:
        import csv
        with open(file_path, newline='', mode='w') as csvfile:
            csv_writer = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
            csv_writer.writerow(infos[0].keys())
            for file_info in infos:
                csv_writer.writerow(file_info.values())

    index_file_path = output_dir.joinpath(".tar_index")
    _write_index(file_infos, index_file_path)

    _create_archive(file_infos,
                    args.output,
                    index_file_path,
                    hash_file_path,
                    threads=args.threads)

    print("Clean up...")

    index_file_path.unlink(missing_ok=True)
    hash_file_path.unlink(missing_ok=True)

    print(f"Archive was written to {args.output}")
    archive_size = args.output.stat().st_size
    scaled_archive_size, archive_size_prefix = auto_prefix_value(archive_size)
    print(
        f"Final archive size: {scaled_archive_size} {archive_size_prefix}B ({round(100 * archive_size / total_size, 3)}%)")


if __name__ == '__main__':
    main()
